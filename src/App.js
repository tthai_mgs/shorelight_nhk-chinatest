import React, { Component } from 'react';
import Amplify,  {Analytics} from 'aws-amplify';
import aws_exports from './aws-exports';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this._handlePinpointClick = this._handlePinpointClick.bind(this);
  }

  componentDidMount() {
    Amplify.configure(aws_exports);
  }

  _handlePinpointClick() {
    Analytics.record('NHK-TEST-PINPOINT-CLICK');
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">NHK Test Page</h1>
        </header>
        <div className="App-buttons">
          <button onClick={this._handlePinpointClick}> Trigger Pinpoint Event </button>
        </div>

      </div>
    );
  }
}

export default App;
